from django.test import TestCase, Client
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from app.users.models import User
# Create your tests here.
class Company1(APITestCase):
    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages=10,
            price='0',
            total_days='14',
            number_of_pos_operators=1,)
        self.package.save()
        self.company=Company.objects.create(
            name ='Inside Company World',
            phone_number = '7278684318',
            package = self.package,
            messages_left = self.package.total_messages,
            is_veryfied=True,
            valid_upto=datetime.date.today())
        self.company.save()
        self.user=User.objects.create_user('pabiowner',password='lala',
            company=self.company,
            is_owner=True,)
        self.user.save()
        self.posuser=User.objects.create_user('pabipos',password='lala',
            company=self.company,
            is_owner=True,)
        self.user.save()
        self.client = APIClient()
        token = Token.objects.get(user=self.user) 
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)



class Company2(APITestCase):
    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages=10,
            price='0',
            total_days='14',
            number_of_pos_operators=1,)
        self.package.save()
        self.company=Company.objects.create(
            name ='Inside2 Comp World',
            phone_number = '7980573770',
            package = self.package,
            messages_left = self.package.total_messages,
            is_veryfied=True,
            valid_upto=datetime.date.today())
        self.company.save()
        self.user=User.objects.create_user('pabi',password='lala',
            company=self.company,
            is_owner=True,)
        self.user.save()
        self.client = APIClient()
        token = Token.objects.get(user=self.user) 
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        def test_company_put(self):
        url = 'http://127.0.0.1:8000/api/v1/company/1/'
        response = self.client.post(
            url,
            data.company['valid'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)