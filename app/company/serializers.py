from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Company,Package
from ..users.models import User


class CompanySerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)
    phone_number = serializers.CharField(read_only=True)
    messages_left = serializers.IntegerField(read_only=True)
    valid_upto = serializers.DateField(read_only=True)
    number_of_pos_operators = serializers.IntegerField(read_only=True)
    class Meta:
        model=Company
        fields=(
            'id',
            'name',
            'address',
            'website',
            'email',
            'phone_number',
            'gstin',
            'gst_rate',
            'messages_left',
            'conversion_rate',
            'valid_upto',
            'number_of_pos_operators',)


class CompanyEditSerializer(serializers.ModelSerializer):
    class Meta:
        model=Company
        fields=(
            'name',
            'address',
            'website',
            'email',
            'gstin',
            'gst_rate',
            'conversion_rate',)        	


class PackageSerialzer(serializers.ModelSerializer):
    class Meta:
        model=Package
        fields=('id','name','total_messages','total_days','price','number_of_pos_operators')


class PosOperatorSerializer(serializers.Serializer):
    id=serializers.CharField(read_only=True)
    username = serializers.CharField()
    password = serializers.CharField()
    company = serializers.CharField(read_only=True)
    email = serializers.EmailField()

    extra_kwargs= {
        'password':{'write_only': True},
    }


class PosOperatorSerializerV1(serializers.Serializer):
    password = serializers.CharField()
    company = serializers.CharField(read_only=True)
    email = serializers.EmailField()

    extra_kwargs= {
        'password':{'write_only': True},
    }


class ComapnyUserSerializer(serializers.Serializer):
    company_name = serializers.CharField()
    company_phone = serializers.CharField()
    email = serializers.EmailField(required=False,allow_null=True,)
    website = serializers.CharField(required=False,allow_null=True,)
    username = serializers.CharField()
    password = serializers.CharField()
    conversion_rate=serializers.FloatField(default=1)
    gst_rate=serializers.FloatField(default=0)
    company = serializers.CharField(read_only=True)

    extra_kwargs = {
        'password': {'write_only': True}
    }


class TokenSerializer(serializers.Serializer):
    token=serializers.CharField()


class OtpSerializer(serializers.Serializer):
    otp=serializers.IntegerField()


class RechargeSerializer(serializers.Serializer):
    package_id=serializers.IntegerField()


class PosIdSerializer(serializers.Serializer):
    pos_id=serializers.UUIDField(format='hex_verbose')


class PhoneNumberSerializer(serializers.Serializer):
    phone=serializers.CharField(
        min_length=10,
        max_length=10
        )
    username=serializers.CharField()


class PasswordSerializer(serializers.Serializer):
    password=serializers.CharField()

#==============================V1=====================================
