from django.contrib import admin
from .models import Company, Package

class CompanyAdmin(admin.ModelAdmin):
	list_display=('id','name','package','messages_left','is_veryfied','valid_upto','number_of_pos_operators')
	search_fields = ('id','name','package',)
admin.site.register(Company,CompanyAdmin)


class PackageAdmin(admin.ModelAdmin):
	list_display=('id','name','total_messages','total_days','number_of_pos_operators')
	search_fields = ('id','name','total_messages','total_days','number_of_pos_operators')
admin.site.register(Package,PackageAdmin)