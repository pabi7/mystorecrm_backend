from rest_framework import permissions

class CompanyEditPermission(permissions.BasePermission):

    message = "Does not have permissions"
    def has_object_permission(self,request,view,obj):
        return obj.users.filter(username=request.user.username).exists() and request.user.is_owner
