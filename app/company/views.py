from django.shortcuts import render
from .models import Package,Company
from .serializers import CompanyEditSerializer,PasswordSerializer,PosOperatorSerializerV1,PhoneNumberSerializer,PosIdSerializer,CompanySerializer,PackageSerialzer,ComapnyUserSerializer,PosOperatorSerializer,TokenSerializer,OtpSerializer,RechargeSerializer
from ..users.serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets , mixins
from rest_framework.views import APIView
from ..users import models
from rest_framework.permissions import AllowAny,IsAdminUser
from app.loyalty.utils import random_with_N_digits,Payment,randomword
from app.loyalty.serializers import MessageSerializer,GetPhoneNumberSerializer
from app.loyalty.permissions import IsOwnerOrNothing,IsSameCompanyOwner,IsActive,PosOperatorLimit,IsCompanyMember, IsVerified, IsOwnerOrReadOnly
from rest_framework.authtoken.models import Token
from datetime import timedelta
import razorpay
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django.conf import settings
import datetime
from ..loyalty.views import CompanyAwareView
from app.loyalty.tasks import send_sms,admin_send_sms
from .permissions import CompanyEditPermission


class CompanyAwareGenericViewSet(viewsets.GenericViewSet):

    def get_queryset(self):
        return super().get_queryset().filter(company=self.request.user.company)


class CreateCompanyUser(APIView):
    permission_classes = (AllowAny,)
    @swagger_auto_schema(request_body=ComapnyUserSerializer ,responses={200: TokenSerializer})
    def post(self,request,format=None):
        otp=random_with_N_digits(5) 
        serializer=ComapnyUserSerializer(data=request.data)
        if serializer.is_valid()==True:
            try:
                company = models.Company.objects.create(
                    name = serializer.data['company_name'],
                    email = serializer.data['email'],
                    phone_number=serializer.data['company_phone'],
                    website = serializer.data['website'],
                    conversion_rate= serializer.data['conversion_rate'],
                    gst_rate= serializer.data['gst_rate'],
                    messages_left=100,
                    otp=otp,
                    valid_upto=datetime.datetime.now()+datetime.timedelta(days=14),
                )
            except Exception as e:
                serializer=MessageSerializer({"message":"Invalid company details"})
                return Response(serializer.data)
            else:
                company.save()
            try:    
                user = models.User.objects.create_user(
                    username=serializer.data['username'],
                    password=serializer.data['password'],
                    phone=serializer.data['company_phone'],
                    company=company,
                    is_owner=True,
                    )
            except Exception as e:
                company.delete()
                print(e)
                serializer=MessageSerializer({"message":"Duplicate at username or phone number!!"})
                return Response(serializer.data)
            else:    
                user.save()
                msg='Here is your OTP: '+str(otp)
                admin_send_sms.delay(company.phone_number, str(msg), user.company)
                token=Token.objects.get(user=user)
                tkn={
                    'token':token.key,
                }
                serializer=TokenSerializer(tkn)
                return Response(serializer.data)
        return Response(serializer.errors)



class OtpCheck(APIView):
    @swagger_auto_schema(request_body=OtpSerializer ,responses={200: TokenSerializer})
    def post(self,request,format=None):
        permission_classes=(IsCompanyMember,IsOwnerOrReadOnly)

        serializer=OtpSerializer(data=request.data)
        if serializer.is_valid()==True:
            if (request.user.company.otp==serializer.data['otp']):
                token=Token.objects.get(user=request.user)
                company=request.user.company
                company.is_veryfied=True
                company.otp=random_with_N_digits(7)
                company.save()
                message={
                    'token': token.key,
                }
            else:
                message={
                    'token': 'invalid otp'
                }
            serializer=TokenSerializer(message)    
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class PosOperatorPasswordSend(APIView):
    permission_classes=(IsOwnerOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=PhoneNumberSerializer ,responses={200: PhoneNumberSerializer})
    def post(self,request,format=None):
        serializer=PhoneNumberSerializer(data=request.data)
        password=randomword(8).lower()
        print(password)
        if serializer.is_valid()==True:
            pos=models.User.objects.create_user(
            company=request.user.company,
            phone=serializer.data['phone'],
            username=serializer.data['username'],
            password=password,
            )
            pos.save()
            msg='your username is: '+str(pos.username)+' .Your one time password is: '+str(password)
            send_sms.delay(pos.phone,msg,request.user.company)
            serializer=PhoneNumberSerializer(pos)
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class PosOperator(APIView):
    @swagger_auto_schema(request_body=PosOperatorSerializer ,responses={200: UserSerializer})
    def post(self,request,format=None):
        permission_classes=(IsOwnerOrNothing,IsVerified,IsActive)
        print(request.user.is_owner)
        company=request.user.company
        if (request.user.is_owner==True and company.number_of_pos_operators>0):
            serializer=PosOperatorSerializer(data=request.data)
            if serializer.is_valid()==True:
                pos=models.User.objects.create_user(
                    username=serializer.data['username'],
                    password=serializer.data['password'],
                    company=request.user.company,
                    email=serializer.data['email'],
                    )
                pos.save()
                company.number_of_pos_operators=company.number_of_pos_operators-1
                company.save()
                serializer=UserSerializer(pos)
                return Response(serializer.data)
            else:
                return Response(serializer.errors)
        else:
            msg={'message':'User should be Owner!'}
            serializer=MessageSerializer(msg)
            return Response(serializer.data)


class PosOperatorViewSet(CompanyAwareView):
    
    queryset=models.User.objects.all()
    serializer_class=UserSerializer
    permission_classes=(IsOwnerOrNothing,IsVerified,IsActive,PosOperatorLimit)


class UserForgotPasswordRequestView(APIView):
    permission_classes = (AllowAny,)
    @swagger_auto_schema(request_body=GetPhoneNumberSerializer ,responses={200: MessageSerializer})
    def post(self,request,format=None):
        serializer=GetPhoneNumberSerializer(data=request.data)
        if serializer.is_valid()==True:
            otp=randint(00000,99999)
            user=models.User.objects.get(
                phone=serializer.data['phone_number']
                )
            print(user)
            user.otp=otp 
            user.save()
            print(user.otp)
            msg='Hi,Your username is:'+str(user.username)+' and your one time password is: '+str(otp)
            send_sms.delay(serializer.data['phone_number'],msg,request.user.company)
            message={"message":"Sms send successfully"}
            serializer=MessageSerializer(message)
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class UserOtpCheck(APIView):
    @swagger_auto_schema(request_body=OtpSerializer ,responses={200: TokenSerializer})
    def post(self,request,format=None):
        permission_classes=(AllowAny,)
        serializer=OtpSerializer(data=request.data)
        if serializer.is_valid()==True:
            if (request.user.otp==serializer.data['otp']):
                token=Token.objects.get(user=request.user)
                message={
                    'token': token.key,
                }
            else:
                message={
                    'token': 'invalid otp'
                }
            serializer=TokenSerializer(message)    
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class ChangePasswordView(APIView):
    @swagger_auto_schema(request_body=PasswordSerializer ,responses={200: UserSerializer}) 
    def post(self,request,format=None):
        permission_classes=(IsCompanyMember,)
        serializer=PasswordSerializer(data=request.data)
        if serializer.is_valid()==True:
           user=request.user
           user.set_password(serializer.data['password'])
           user.save()
           user_serializer=UserSerializer(user)
           return Response(user_serializer.data)
        else:
            return Response(serializer.errors)


class PackageViewSet(mixins.RetrieveModelMixin,
                        mixins.ListModelMixin,
                        viewsets.GenericViewSet):    
    queryset=Package.objects.all()
    serializer_class= PackageSerialzer
    permission_classes = (IsOwnerOrNothing,IsVerified,)


class CompanyEditviewset(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        viewsets.GenericViewSet):
    """
        Put: 
            
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (IsVerified,CompanyEditPermission)


class CompanyDetailsView(APIView):
    @swagger_auto_schema(responses={200: CompanySerializer})
    def get(self,request,format=None):
        permission_classes=(IsOwnerOrNothing,)
        company=request.user.company
        serializer=CompanySerializer(company)
        return Response(serializer.data)


class ChangeSmsPack(APIView):
    permission_classes=(IsOwnerOrNothing,IsVerified,)
    
    @swagger_auto_schema(request_body=RechargeSerializer ,responses={200: CompanySerializer})
    def post(self,request,payment_id,format=None):
        serializer=RechargeSerializer(data=request.data)
        if serializer.is_valid()==True:
            pack=Package.objects.get(id=serializer.data['package_id'])
            company=request.user.company
            price=pack.price*100
            res=Payment.capture_a_payment(self,payment_id,price)
            if (res['amount']==price and res['status']=='captured'):      #will impliment error_code==None
                if company.messages_left==None:
                    company.messages_left=pack.total_messages
                else:
                    company.messages_left=int(company.messages_left)+int(pack.total_messages)
                days=company.valid_upto
                if days<=datetime.date.today():
                    company.valid_upto=datetime.date.today()+datetime.timedelta(days=int(pack.total_days)-1)
                else:
                    company.valid_upto=days+datetime.timedelta(days=int(pack.total_days)-1)
                company.number_of_pos_operators=pack.number_of_pos_operators
                company.package=pack
                company.save()
                msg='recharge successfull of amount '+str(pack.price)
                admin_send_sms.delay(company.phone_number,msg,request.user.company)
                serializer=CompanySerializer(company)
                return Response(serializer.data)
            else:
                return Response ({"message":"recharge failed!"})
        else:
            return Response(serializer.errors)
#above is optimizable
