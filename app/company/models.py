from django.db import models
from django.db.models.signals import pre_save,post_save
import datetime

class Package(models.Model):
    #Attributes-Mandetory
    total_messages=models.IntegerField()
    price=models.FloatField()
    total_days=models.IntegerField()
    number_of_pos_operators=models.IntegerField(default=0)
    #Attributes-Optional
    name=models.CharField(
        max_length=20,
        blank=True
        )

    #Methods
    def __str__(self):
        return str(self.id)


class Company(models.Model):
    #Relations
    package=models.ForeignKey(
        Package,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    
    #Attibutes - Mandatory
    name = models.CharField(
        max_length=100,)

    #Attributes - Optional
    address = models.TextField(
        blank=True,
        )
    website = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        )
    email=models.EmailField(
        blank=True,
        null=True,
        )
    phone_number = models.CharField(
        max_length=10,
        unique=True,
        )
    gstin= models.CharField(
        max_length=17,
        blank=True,
        )
    messages_left = models.IntegerField(
        default=0,
        )
    conversion_rate = models.FloatField(
        default=1,
        blank=True,
        )
    valid_upto=models.DateField(
        blank=True,
        )
    otp=models.IntegerField(
        null=True,
        blank=True,
        )
    is_veryfied=models.BooleanField(
        default=False
        )
    gst_rate=models.FloatField(
        default=0,
        blank=True,
        )
    number_of_pos_operators=models.IntegerField(
        default=0,
        blank=True,
        )

    #Methods
    def __str__(self):
        return str(str(self.name)+'('+str(self.id)+')')

    '''def finish(self):
        self.valid_upto=datetime.date.today()
        #self.save()

def validity_check(sender,**kwargs):
    if kwargs['created']:
        company= Company(
            valid_upto=datetime.date.today()+datetime.timedelta(days=14))
        #company.save()'''


#post_save.connect(validity_check,sender=Company)

class Store(models.Model):
    #Relations
    company=models.ForeignKey(
        Company,
        on_delete=models.CASCADE
    )
    #Attributes-Mandatory
    address_line1 = models.CharField(
        max_length=100,
    )
    address_line2 = models.CharField(
        max_length=100,
        blank=True
    )
    city = models.CharField(
        max_length=50
    )
    state = models.CharField(
        max_length=50
    )
    pin = models.IntegerField(
        default=100
    )
    phone_number = models.CharField(
        max_length=10,
        unique=True,
        )
    
#Methods
    def __str__(self):
        return str(str(self.name)+'('+str(self.id)+')')

