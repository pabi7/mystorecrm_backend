import random, string

class CurrentUserDefault(object):
    """
     Class to get current user in serilizer
    """
    def set_context(self, serializer_field):
        self.user = serializer_field.context['request'].user

    def __call__(self):
        return self.user

    def __repr__(self):
        return unicode_to_repr('%s()' % self.__class__.__name__)
