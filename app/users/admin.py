from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User

class UserAdmin(admin.ModelAdmin):
	list_display=('username','company','is_owner')


admin.site.register(User,UserAdmin)
'''@admin.register(User)
class UserAdmin(UserAdmin):
    pass'''
