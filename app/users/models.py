import uuid
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
from django.utils.encoding import python_2_unicode_compatible
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from app.company.models import Company
from app.loyalty.models import Transaction,Customer


@python_2_unicode_compatible
class User(AbstractUser):
    company=models.ForeignKey(Company,
        related_name='users',
        null=True,
        on_delete=models.CASCADE)
        
    # Attribute - Mandatary
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    phone = models.CharField(
            max_length=10,
            null=True,
            unique=True
            )
    # Attributte - Optional
    website = models.TextField(
            blank=True,
            null=True,)
    
    email = models.EmailField(
            blank=True,
            null=True,
            )
    is_owner = models.BooleanField(
            blank=True,
            default=False,
            )
    otp = models.CharField(
        max_length=10,
        blank=True
        )

    is_veryfied=False

    points=models.IntegerField(
            default=0
            )
    
    #Methods
    def __str__(self):
        return 'Username:'+str(self.username)+' id: '+str(self.id)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=Customer)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        User.points=User.points+1
        User.save()


@receiver(post_save, sender=Transaction)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        User.points=User.points+1
        User.save()