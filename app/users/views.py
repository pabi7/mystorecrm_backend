from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from .models import User
from .permissions import IsUserOrReadOnly
from .serializers import CreateUserSerializer, UserSerializer
from app import loyalty
from rest_framework.views import APIView

class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsUserOrReadOnly,)


class UserCreateViewSet(mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    """
    Creates user accounts
    """
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = (AllowAny,)


class OpreatorDashboard(APIView):
    def get(self,request,format=None):
        serializer=OperatorDashboardSerializer(data=request.data)
        if serializer.is_valid()==True:
            customers=loyalty.models.Customer.objects.filter(
                company=request.user.company,
                user=request.user,
                ).count()    
            transactions=models.transaction.objects.filter(
                company=request.user.company,
                user=request.user,
                ).count()
            totalSaleAmount=models.Cart.objects.aggregate(total_sale_amount=Sum('totalprice'))
            data={
                'customers':customers,
                'transaction':transactions,
                'totalSaleAmount':totalSaleAmount,
                }
            serializer = OperatorDashboardSerializer(data)
            return Response (serializer.data)
        else:
            return Response(serializer.errors)        
        
    
