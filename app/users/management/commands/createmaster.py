from django.core.management.base import BaseCommand, CommandError
from ...models import User 

class Command(BaseCommand):
    """
        Command for creating master
    """
    def handle(self, *args, **options):
        if not User.objects.filter(username="admin").exists():
            User.objects.create_superuser("admin", "sappy@spotings.com","lalaland123")
            self.stdout.write(self.style.SUCCESS('Successfully created new super user'))
        else:
            self.stdout.write(self.style.ERROR('Admin already created'))
