from rest_framework.test import APITestCase
from .models import User
import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from .serializers import UserSerializer,CreateUserSerializer
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from django.conf import settings
from rest_framework.test import APIClient
from .views import UserCreateViewSet,UserViewSet
from rest_framework.authtoken.models import Token


def UserTestCase(APITestCase):

    def setUp(self):
        self.client = APIClient()
        token = Token.objects.get(user=self.user) 
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def create_user_test_case(self):
        url='http://127.0.0.1:8000/api/v1/user/'
        data={
            'username':'pabi', 
            'first_name':'Pabiy', 
            'last_name':'Roy',
            }
        response = self.client.post(
            url,
            data, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def retrive_user_test_case(self):

        url='http://127.0.0.1:8000/api/v1/user/'
        data={
            'username':'pabi', 
            'first_name':'Pabiy', 
            'last_name':'Roy',
            }
        self.client.post(
            url,
            data, 
            format='json')
        user=User.objects.get(username=pabi)
        url='http://127.0.0.1:8000/api/v1/user/'+str(user.id)+'/'
        response = self.client.get(
            url,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def update_user_test_case(self):
        url='http://127.0.0.1:8000/api/v1/user/'
        data={
            'username':'pabi', 
            'first_name':'Pabiy', 
            'last_name':'Roy',
            }
        self.client.post(
            url,
            data, 
            format='json')
        user=User.objects.get(username=pabi)
        url='http://127.0.0.1:8000/api/v1/user/'+str(user.id)+'/'
        data={
            'username':'pabi', 
            'first_name':'duydu', 
            'last_name':'Das',
            }
        response = self.client.Put(
            url,
            data,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



        


