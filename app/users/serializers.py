from rest_framework import serializers
from .models import User
from ..company.models import Company


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'company', 
            'username',
            'points',
            'phone', 
            'first_name', 
            'last_name',
            'email',)
        read_only_fields = ('username', )


class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        # call create_user on user object. Without this
        # the password will be stored in plain text.
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = (
            'id', 
            'company',
            'username', 
            'password', 
            'first_name', 
            'last_name', 
            'email', 
            'auth_token',)
        read_only_fields = ('auth_token',)
        extra_kwargs = {'password': {'write_only': True}}


class OperatorDashboardSerializer(serializers.Serializer):
    customers=serializers.IntegerField(read_only=True)
    transactions=serializers.IntegerField(read_only=True)
    totalSaleAmount=serializers.FloatField(read_only=True)