from django.conf import settings
from django.urls import path, re_path, include, reverse_lazy
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView
from rest_framework import permissions
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views as v
from .users.views import UserViewSet, UserCreateViewSet
from rest_framework_swagger.views import get_swagger_view
from .loyalty import views
from .company import views as companyview
from .users import views as userview
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from app.promotion import views as promoview

#schema_view = get_swagger_view(title='SpotCRM')
schema_view = get_schema_view(
   openapi.Info(
      title="MystoreCRM",
      default_version='v1',
      description="A api for MystoreCRM",
      terms_of_service="https://mystorecrm.com/",
      contact=openapi.Contact(email="support@mystorecrm.com"),
      license=openapi.License(name="copyright@ Spotings"),
   ),
   validators=['flex', 'ssv',],
   public=True,
   permission_classes=(permissions.AllowAny,)
)

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'users', UserCreateViewSet)
router.register(r'customer', views.CustomerViewSet)
router.register(r'offer', views.OfferViewSet)
router.register(r'items', views.ItemViewSet)
router.register(r'package', companyview.PackageViewSet)
router.register(r'pos', companyview.PosOperatorViewSet)
router.register(r'category',views.CategoryViewSet)
router.register(r'followup',promoview.FollowUpViewSet)
router.register(r'company',companyview.CompanyEditviewset)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/customer/<int:pk>/followup/timeline/', promoview.CustomerFollowUpTimeline.as_view()),
    path('api/v1/customer/<int:pk>/requirment/', promoview.RequirmentView.as_view()),
    path('api/v1/user/dashboard/', userview.OpreatorDashboard.as_view()),
    path('api/v1/company/', companyview.CompanyDetailsView.as_view()),
    path('api/v1/customer/<int:pk>/note/' , views.NoteView.as_view()),
    path('api/v1/categorymgmt/<int:pk>/',views.CategoryManagement.as_view()), 
    #create user + company
    path('api/v1/register/', companyview.CreateCompanyUser.as_view()),
    #add pos operator (v0.2(beta))
    path('api/v1/posoperator/', companyview.PosOperator.as_view()),
    #otp test
    path('api/v1/otpcheck/', companyview.OtpCheck.as_view()),
    #user otp check
    path('api/v1/otpcheck/user/', companyview.UserOtpCheck.as_view()),
    #Recharge
    path('api/v1/recharge/<str:payment_id>/',companyview.ChangeSmsPack.as_view()),
    #pos otp check
    path('api/v1/posregister/',companyview.PosOperatorPasswordSend.as_view()),
    #path('api/v1/notarrived/',views.CustomerDidnotArrived.as_view()),
    path('company/<int:cpk>/transaction/<int:pk>/',views.getPdfView),
    #path('offer/<int:pk>/',ve.offer_detail,name='offer_details'),
    path('api/v1/password/forgot/', companyview.UserForgotPasswordRequestView.as_view()),
    path('api/v1/password/reset/',companyview.ChangePasswordView.as_view()),
    path('api/v1/sms/', views.SendSmsToSelected.as_view()),
    path('api/v1/customer/<int:pk>/sms/',views.SendSmstoPerticularCustomerView.as_view()),
    path('api/v1/customsms/', views.CampaignView.as_view()),
    path('api/v1/customsms/preview/',views.CampaignPreviewView.as_view()),
    path('api/v1/customer/<int:pk>/product/', views.ProductSuggest.as_view()),
    path('api/v1/customer/<int:pk>/quote/', views.QuoteView.as_view()),
    path('api/v1/customer/<int:pk>/redeem/',views.RedeemRequestView.as_view()),
    path('api/v1/customer/<int:pk>/redeem/otp/', views.RedeemView.as_view()),
    path('api/v1/customer/phone',views.GetPhoneNumber.as_view()),
    path('api/v1/sms/all/',views.SendAllSMSView.as_view()),
    path('api/v1/customer/<int:pk>/transaction/',views.TransactionView.as_view()),
    path('api/v1/bill/', views.BillManagement.as_view()),
    path('api/v1/stat/', views.DashboardView.as_view()),
    path('api/v1/customer/<int:pk>/offer/',views.CustomerOfferList.as_view()),
    path('api/v1/customer/<int:pk>/sendoffer/', views.TopOffersSendtoACustomer.as_view()),
    path('api-token-auth/', v.obtain_auth_token),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #path('api/graph',schema_view),
    #url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    #path('api/v1/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/v1/', include(router.urls)),
    path('', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    re_path(r'^$', RedirectView.as_view(url=reverse_lazy('api-root'), permanent=False)),
    path('api/v1/', include(router.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

