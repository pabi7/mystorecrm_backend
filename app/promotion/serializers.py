from rest_framework import serializers
from app.loyalty.serializers import CompanyAwareSerializer
from . import models
from app.users.utils import CurrentUserDefault
from app.company.utils import CurrentCompanyDefault

class CompanyUserAwareSerializer(serializers.ModelSerializer):
	company = serializers.HiddenField(
            default=CurrentCompanyDefault()
            )
	user = serializers.HiddenField(
            default=CurrentUserDefault()
            )


class FollowUpSerializer(CompanyUserAwareSerializer):
	status=serializers.ChoiceField(
		choices=(
		'Not followed up',
		'Expected arrival at',
		'Not interested',
		'Unreachable',
		'Not answared',)
		)
	class Meta:
		model = models.FollowUp
		fields = (
			'customer',
			'user',
			'date_time',
			'note',
			'status',
			'company',
		)


class RequirmentSerializer(serializers.Serializer):
	items=serializers.ListField(
        child=serializers.IntegerField(),
        default=None,
    )
