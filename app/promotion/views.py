from django.shortcuts import render
from app.loyalty.views import UserAwareView
from . import models
from app import loyalty
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from . import serializers
from rest_framework.response import Response
# Create your views here.

class FollowUpViewSet(UserAwareView):

    queryset=models.FollowUp.objects.filter(status='Not Followed Up')
    serializer_class=serializers.FollowUpSerializer
    permisson_classes=(
        loyalty.permissions.IsActive,
        loyalty.permissions.IsCompanyMember,
        loyalty.permissions.IsVerified
        )


class FollowUpDoneView(APIView):
    permisson_classes=(
        loyalty.permissions.IsActive,
        loyalty.permissions.IsCompanyMember,
        loyalty.permissions.IsVerified
        )
    def post(self,request,pk,format=None):
        followup=models.FollowUp.objects.get(id=pk)
        followup.is_follwedUp=True
        serializer=loyalty.serializers.MessageSerializer({"message":"Followup Done"})
        return Response(serializer.data)


class CustomerFollowUpTimeline(APIView):
    permisson_classes=(
        loyalty.permissions.IsActive,
        loyalty.permissions.IsCompanyMember,
        loyalty.permissions.IsVerified
        )
    def get(self,request,pk,format=None):
        customer=loyalty.models.Customer.objects.get(id=pk)
        followUps=customer.followup_set.filter(
            company=request.user.company,
            customer=customer,)
        serializer=serializers.FollowUpSerializer(followUps, many=True)
        return Response(serializer.data)


class RequirmentView(GenericAPIView):
    permisson_classes=(
        loyalty.permissions.IsActive,
        loyalty.permissions.IsCompanyMember,
        loyalty.permissions.IsVerified
        )
    def post(self,request,pk,format=None):
        requirment_serializer=serializers.RequirmentSerializer(data=request.data)
        if requirment_serializer.is_valid()==True:
            company=request.user.company
            customer=loyalty.models.Customer.objects.get(id=pk)
            follwUpserializer=serializers.FollowUpSerializer(data=request.data)
            if follwUpserializer.is_valid()==True:
                follwUp=models.FollowUp.objects.create(
                    company=company,
                    user=request.user,
                    customer=customer,
                    note=follwUpserializer.data['note'],
                    date=follwUpserializer.data['date_time']
                    )
                follwUp.save()
                for itemId in requirment_serializer.data['items']:
                    item=loyalty.models.Item.objects.get(id=itemId)
                    requirment=models.Requirment.objects.create(
                        company=company,
                        customer=customer,
                        followUp=follwUp,
                        )
                    requirment.save()
                    requirment.item.add(item)
                    requirment.save()
                return Response(follwUpserializer.data)
            else:
                return Response(follwUpserializer.errors)
        else:
            return Response(requirment_serializer.errors)