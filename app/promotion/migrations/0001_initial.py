# Generated by Django 2.0.7 on 2019-02-19 13:23

import datetime
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('company', '0004_auto_20190219_1853'),
        ('loyalty', '0004_auto_20190208_1415'),
    ]

    operations = [
        migrations.CreateModel(
            name='FollowUp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('date', models.DateTimeField(default=datetime.datetime(2019, 2, 19, 18, 53, 52, 197666))),
                ('note', models.TextField(blank=True, max_length=500)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Company')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loyalty.Customer')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
