from django.db import models
from app import loyalty,company
from django.contrib.auth.models import User
import datetime
from django.conf import settings
# Create your models here.

class FollowUp(loyalty.models.CompanyAwareModel):
    #Relations
    customer = models.ForeignKey(
        loyalty.models.Customer,
        on_delete=models.CASCADE
        )
    user=models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
        )
    #Attributes-Mandatory
    date_time = models.DateTimeField(
        default=None,
        )
    note = models.CharField(
        max_length=500,
        blank=True)
    
    status = models.CharField(
        max_length=20,
        default='Not Followed Up'
        )
    #Meta and strings
    def __str__(self):
        return str(self.id)

    #Extra functions
    def save_model(self, request, obj, form, change):
        if not obj.pk:
        # Only set added_by during the first save.
            obj.user = request.user
        super().save_model(request, obj, form, change)


class Requirment(loyalty.models.CompanyAwareModel):
    #Relations
    customer = models.ForeignKey(
        loyalty.models.Customer,
        on_delete=models.CASCADE
        )
    item = models.ManyToManyField(
        loyalty.models.Item,
        )
#Meta and strings
    def __str__(self):
        return str(self.id)
