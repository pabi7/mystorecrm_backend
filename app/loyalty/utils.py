from random import randint
import http 
from ..company.models import Company
import razorpay
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.conf import settings
import random, string

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def randomword(length):
   letters = string.ascii_uppercase
   return ''.join(random.choice(letters) for i in range(length)) 

def customized_sender_name(comp):
    company=str(comp)
    if len(company)> 6:
        name=company[:6]
        return name.upper()
    else:
         return (str(company)+str(randomword((6-len(company))))).upper()

def send_sms(phone_number,message,company):
    company_messages = company.messages_left
    if (company_messages>0):
        conn = http.client.HTTPConnection("api.msg91.com")
        msg=str(message)
        conn.request("GET","/api/sendhttp.php?sender="+str(customized_sender_name(company.name))+"&route=4&mobiles="+str(phone_number)+"&authkey="+str(settings.MSG91_KEY)+"&country=91&message="+str(msg))
        res = conn.getresponse()
        data = res.read()
        company.messages_left=int(company_messages)-1
        company.save()
        return data.decode("utf-8")
    else:
        raise Exception ("No messages left, Please recharge a SMS pack!")
        
def admin_send_sms(phone_number,message,company):
    conn = http.client.HTTPConnection("api.msg91.com")
    msg=str(message)
    conn.request("GET","/api/sendhttp.php?sender=MYSTOR&route=4&mobiles="+str(phone_number)+"&authkey="+str(settings.MSG91_KEY)+"&country=91&message="+str(msg))
    res = conn.getresponse()
    data = res.read()
    return data.decode("utf-8")

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None

class Payment:
    def fetch_all_payments(self):
        return (self.client.payment.all())

    def fetch_a_payment(self,payment_id):
        return(self.client.payment.fetch(payment_id))

    def capture_a_payment(self,payment_id,amount):
        '''
            Note: <AMOUNT> should be same as the original amount
            while creating the payment
        '''
        self.client = razorpay.Client(auth=(settings.RAZORPAY_KEY_ID, settings.RAZORPAY_KEY_SECRET))
        self.client.set_app_details({"title" : "Django", "version" : "2.0.7"})
        return(self.client.payment.capture(payment_id, amount))

    def refund_a_payment(self,payment_id,amount):
        '''
        Note: <AMOUNT_TO_BE_REFUNDED> should be equal/less than 
        the original amount
        '''
        client.payment.refund(payment_id, amount)
        # for full refund
        client.payment.refund(payment_id, amount)
        # for particular amount

    def bank_transfer(payment_id):
        client.payment.bank_transfer(payment_id)

    def transfer_to_given_payment_id(payment_id):
        client.payment.transfer(payment_id)


# Python3 program to find the most 
# frequent element in an array. 

def mostFrequent(arr): 
    n = len(arr) 
    # Sort the array 
    arr.sort() 

    # find the max frequency using 
    # linear traversal 
    max_count = 1; res = arr[0]; curr_count = 1
    
    for i in range(1, n): 
        if (arr[i] == arr[i - 1]): 
            curr_count += 1
            
        else : 
            if (curr_count > max_count): 
                max_count = curr_count 
                res = arr[i - 1] 
            
            curr_count = 1
    
    # If last element is most frequent 
    if (curr_count > max_count): 
    
        max_count = curr_count 
        res = arr[n - 1] 
    
    return res
