from django.contrib import admin
from . import models

class CustomerAdmin(admin.ModelAdmin):
	list_display=('id','phone_number','company','points','otp',)
	search_fields = ('id','phone_number','company__name')
admin.site.register(models.Customer,CustomerAdmin)


class OfferAdmin(admin.ModelAdmin):
	list_display=('id','company','about','points',)
admin.site.register(models.Offer,OfferAdmin)


class MessageAdmin(admin.ModelAdmin):
	list_display=('id','company','text',)
admin.site.register(models.Message,MessageAdmin)


class CartAdmin(admin.ModelAdmin):
	list_display=('id','customer','company','transaction','item','miscellaneous_items')
	search_fields = ('id','customer','company','transaction','item')
admin.site.register(models.Cart,CartAdmin)


class TransactionAdmin(admin.ModelAdmin):
	list_display=('id','customer','company')
	search_fields = ('id','customer','company')
admin.site.register(models.Transaction,TransactionAdmin)


class ItemAdmin(admin.ModelAdmin):
	list_display=('company','id','name','price','quantity','unit')
	search_fields = ('company','id','name','price','quantity','unit')
admin.site.register(models.Item,ItemAdmin)


class CategoryAdmin(admin.ModelAdmin):
	list_display=('id','company','name')
	search_fields = ('id','company','name')
admin.site.register(models.Category,CategoryAdmin)


class NoteAdmin(admin.ModelAdmin):
	list_display=('id','note','customer')
	search_fields = ('id','note','customer')
admin.site.register(models.Note,NoteAdmin)


