from django.contrib.auth.models import User, Group
from rest_framework import serializers
from . import models
from ..users.serializers import UserSerializer
from ..company.utils import CurrentCompanyDefault
# class CompanySerializer(serializers.ModelSerializer):
    # """ 
        # Geneneric Customer Serializer
    # """
    # class Meta:
        # model = Company
        # fields = (
            # 'name',
            # 'team',
            # 'phone_number',
            # 'address',
            # 'website',)

def isPositiveNumber(value):
        if value < 0:
            raise serializers.ValidationError('This field must be an positive number.')


class CompanyAwareSerializer(serializers.ModelSerializer):

    company = serializers.HiddenField(
            default=CurrentCompanyDefault()
            )



class CustomerSerializer(CompanyAwareSerializer):

    """
        Generic Customer Serializer
    """
   
    class Meta:
        model = models.Customer
        fields = (
            'id',
            'company',
            'user',
            'phone_number',
            'username',
            'email',
            'points',)

    
class OfferSerializer(CompanyAwareSerializer):
    points = serializers.FloatField(validators=[isPositiveNumber])
    class Meta:
        model = models.Offer
        fields = (
            'id',
            'company',
            'about',
            'points',
            )


# class RedeemSerializer(CompanyAwareSerializer):
    # customer_id = serializers.IntegerField()
    # offer = serializers.IntegerField()
    # otp = serializers.IntegerField(write_only=True)


class MessageSerializer(serializers.Serializer):
    message = serializers.CharField(read_only=True)
    error = serializers.CharField(read_only=True)


class CategorySerializer(CompanyAwareSerializer):
    id=serializers.CharField(read_only=True)
   # company=serializers.CharField(read_only=True)
    
    class Meta:
        model=models.Category
        fields=(
            'id',
            'name',
            'company',
            )


class ItemSerializer(CompanyAwareSerializer):
    category=CategorySerializer(required=False)
    class Meta:
        model = models.Item
        fields = (
            'id',
            'name',
            'company',
            'category',
            'price',
            'quantity',
            'unit',)
        
    def create(self, validated_data):
        category_data = validated_data.pop('category')
        category = models.Category.objects.get(name=category_data["name"])
        item=models.Item.objects.create(category=category, **validated_data)
        return item
    
    def update(self,instance,validated_data):
        category_data = validated_data.pop('category')
        category = models.Category.objects.get(name=category_data["name"])
        instance.name = validated_data.get('name', instance.name)
        instance.price = validated_data.get('price', instance.price)
        instance.unit = validated_data.get('unit', instance.unit)
        instance.quantity= validated_data.get('quantity', instance.quantity)
        instance.category=validated_data.get('category', category)
        instance.save()
        return instance


class BuySerializer(serializers.Serializer):
    id=serializers.CharField(read_only=True)
    price = serializers.FloatField(validators=[isPositiveNumber])



class TotalCustomerSerializer(serializers.Serializer):
    total_customer=serializers.IntegerField(read_only=True)
    monthly_total_customer=serializers.IntegerField(read_only=True)
    daily_total_customer=serializers.IntegerField(read_only=True)
    weekly_total=serializers.IntegerField(read_only=True)
    monthly_total_sale_amount=serializers.FloatField(read_only=True)
    weekly_total_sale=serializers.FloatField(read_only=True)
    daily_total_sale=serializers.FloatField(read_only=True)
    total_sale_amount=serializers.FloatField(read_only=True)


class GetPhoneNumberSerializer(serializers.Serializer):
    phone_number=serializers.CharField(max_length=10, min_length=10)


class TransactionInputSerializer(serializers.Serializer):
    items=serializers.ListField(
        child=serializers.DictField(
            child=serializers.IntegerField(),
        ),
        default=None,
    )
    price=serializers.ListField(
        child=serializers.FloatField(),
        default=None,
    )
    gst_rate=serializers.FloatField(read_only=True,)
    company=serializers.CharField(read_only=True,)


class TransactionResponseSerializer(serializers.Serializer):
    transaction_id=serializers.IntegerField() #to be charized


class TransactionOutSerializer(serializers.Serializer):
    transaction_id=serializers.CharField()
    company=serializers.CharField()
    customer=serializers.CharField()
    items=serializers.JSONField()
    miscellaneous=serializers.JSONField()
    customer_points=serializers.FloatField()
    total_gst=serializers.FloatField()
    total_price_before_GST=serializers.FloatField()
    total_price_after_GST=serializers.FloatField()
    created=serializers.DateTimeField()
    url=serializers.CharField()
    
    
class SendAllSmsSerializer(serializers.Serializer):
    sms=serializers.CharField()


class SendSmsToSelectedSerializer(serializers.Serializer):
    max_points=serializers.IntegerField(default=None)
    min_points=serializers.IntegerField(default=None)


class RedeemRequestSerializer(serializers.Serializer):
    offer_id=serializers.IntegerField()


class RedeemSerializer(serializers.Serializer):
    otp=serializers.CharField()


class TransactionIdSerializer(serializers.Serializer):
    transaction_id=serializers.IntegerField()


class MiscellaneousSerializer(CompanyAwareSerializer):
    class Meta:
        model=models.Miscellaneous
        fields=(
            'id',
            'company',
            'price',
            )


class CampaignSerializer(serializers.Serializer):
    max_points=serializers.FloatField(default=None)
    min_points=serializers.FloatField(default=None)
    customer_absent=serializers.CharField(default=None)
    #items_bought=serializers.ListField(
    #    child=serializers.IntegerField(),
    #    default=None,
    #)
    sms=serializers.CharField(default=None)


class CampaignPreviewSerializer(serializers.Serializer):
    max_points=serializers.FloatField(default=None)
    min_points=serializers.FloatField(default=None)
    customer_absent=serializers.CharField(default=None)
    #items_bought=serializers.ListField(
    #    child=serializers.IntegerField(),
    #    default=None,
    #)


class TransactionSerializer(CompanyAwareSerializer):
    class Meta:
        model=models.Transaction
        fields=(
            'id',
            'company',
            'customer',
            'created',
        )


class CampaignOutSerializer(serializers.Serializer):
    absent_customer=serializers.IntegerField()
    #total_customer=serializers.IntegerField()
    #point_based_selected=serializers.IntegerField()
    #company=serializers.CharField(read_only=True)


class SendSmstoPerticularCustomerSerializer(serializers.Serializer):
    phone_number=serializers.CharField()
    sms=serializers.CharField()


class ItemIdSerializer(serializers.Serializer):
    item_id=serializers.IntegerField()



class NoteSerializer(CompanyAwareSerializer):
    customer=serializers.CharField(read_only=True)
    class Meta:
        model=models.Note
        fields=(
            'note',
            'customer',
            'created',
        )
