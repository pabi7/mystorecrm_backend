from django.db import models
from django.conf import settings
from model_utils.models import TimeStampedModel
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator
import uuid
from ..company.models import Company
#from phonenumber_field.modelfields import PhoneNumberField
from django.core.validators import RegexValidator
import datetime
from django.db.models.signals import pre_save

class CompanyAwareModel(TimeStampedModel):
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Offer(CompanyAwareModel):
    #Attributes - Mandatory
    about = models.TextField()
    points =models.IntegerField(
        default=0
    )
    #Methods
    def __str__(self):
        return str('About: '+str(self.about)+', Id:'+str(self.id))


class Customer(CompanyAwareModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default=0-0-0,
        on_delete=models.CASCADE
        )
    #Attributes - Mandatory
    phone_number = models.CharField(
        max_length=10,
        blank=True, )
    #Attributes - Optional 
    username = models.CharField(
        max_length=100,
        blank=True,)
    email = models.EmailField(
        max_length=70, 
        blank=True,
        null=True)
    points= models.FloatField( 
        blank=True,
        null=False,
        default=0)
    otp = models.CharField(
        max_length=6,
        null=True,
        blank=True,)
    offer_to_redeem = models.ForeignKey(
        Offer,
        blank=True,
        null=True,
        on_delete=models.CASCADE,)

    # Methods and Meta
    def __str__(self):
        return str('Phone: '+str(self.phone_number)+', Id:'+str(self.id))


    class Meta:
        unique_together = ('company','phone_number')

    


    

class Message(CompanyAwareModel):
    #Attributes - Mandatory
    text = models.TextField()

    #Methods
    def __str__(self):
        return str(self.id)


class Category(CompanyAwareModel):
    #Attributes - Mandatory    
    name=models.CharField(
        max_length=20,
        )

    #Methods
    def __str__(self):
        return str('Name: '+str(self.name)+', Id:'+str(self.id))


class Item(CompanyAwareModel):
    #Relations
    category=models.ForeignKey(
        Category,
        blank=True,
        null=True,
        on_delete=models.CASCADE
        )
    #Attributes - Mandatory
    price = models.FloatField()

    #Attributes - Optional
    name = models.CharField(
        max_length=25,
        blank=True,
        unique=True)
    quantity=models.IntegerField(null=True,blank=True)

    unit=models.CharField(
        max_length=10,
        )

    #Methods
    def __str__(self):
        return str('Name: '+str(self.name)+', Id: '+str(self.id))


class Miscellaneous(CompanyAwareModel):
    #Attributes-optional
    price=models.FloatField(unique=True)

     #Methods
    def __str__(self):
        return str('Price: '+str(self.price)+', Id: '+str(self.id))


class Transaction(CompanyAwareModel):
    #Relations
    customer=models.ForeignKey(
        Customer,
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default=0-0-0,
        on_delete=models.CASCADE
        )
    is_deleted=models.BooleanField(
            blank=True,
            default=False,
            )
    #Methods
    def __str__(self):
        return str(self.id)


class Cart(CompanyAwareModel):
    #Relations
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default=0-0-0,
        on_delete=models.CASCADE
        )
    customer = models.ForeignKey(
        Customer, 
        on_delete=models.CASCADE)
    item = models.ForeignKey(
        Item,
        blank=True,
        null=True,
        on_delete=models.CASCADE)

    miscellaneous_items = models.ForeignKey(
        Miscellaneous,
        blank=True,
        null=True,
        on_delete=models.CASCADE)

    transaction=models.ForeignKey(
        Transaction,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )

    quantity=models.IntegerField(
        default=0,
        blank=True
        )
    #Attributes-Optional
    totalprice=models.FloatField(
        default=0,
        blank=True,
    )
    #Methods
    def __str__(self):
        return str(self.id)


class Note(CompanyAwareModel):
    #Relations-Mandatory
    customer = models.ForeignKey(
        Customer, 
        on_delete=models.CASCADE)
    #Attributes-Mandatory
    note=models.CharField(max_length=200)

    #Methods
    def __str__(self):
        return str(self.id)
