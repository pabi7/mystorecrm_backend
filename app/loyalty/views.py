from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .utils import render_to_pdf,mostFrequent
from . import models
import random
from ..company import models as cmodels
from django.conf import settings
from rest_framework.generics import GenericAPIView
from .serializers import (
 NoteSerializer,
 CategorySerializer,
 ItemIdSerializer,
 SendSmstoPerticularCustomerSerializer,
 CampaignOutSerializer,
  CampaignSerializer,
  CampaignPreviewSerializer,
  TransactionIdSerializer,
  TransactionSerializer,
  CustomerSerializer, 
  OfferSerializer, 
  RedeemRequestSerializer,
  MessageSerializer,
  ItemSerializer,
  RedeemSerializer,
  TransactionInputSerializer,
  TransactionOutSerializer,
  TransactionResponseSerializer,
  SendSmsToSelectedSerializer,
  SendAllSmsSerializer,
  TotalCustomerSerializer,
  GetPhoneNumberSerializer,
  MiscellaneousSerializer,)
from django.http import HttpResponse
from ..users.permissions import IsUserOrReadOnly
from .permissions import IsCompanyMember,IsSameCompanyOwner,IsVerified,IsOwnerOrReadOnly,IsActive
from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from random import randint
from rest_framework.pagination import PageNumberPagination
from app.loyalty.tasks import send_sms,admin_send_sms,urlShort
from rest_framework import permissions
from django.db.models import Avg, Count, Min, Sum, Q, F
import datetime
import razorpay
import logging
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.settings import api_settings
from drf_yasg.utils import swagger_auto_schema
import requests
#from app.config.common import DEFAULT_PAGINATION_CLASS


logger = logging.getLogger(__name__)


class CompanyAwareView(viewsets.ModelViewSet):

    def get_queryset(self):
        return super().get_queryset().filter(company=self.request.user.company)


class UserAwareView(viewsets.ModelViewSet):
    def get_queryset(self):
        return super().get_queryset().filter(
            company=self.request.user.company,
            user=self.request.user)


class CustomerViewSet(UserAwareView):
    """
    retrieve:
    Return the given customer.

    list:
    Return a list of all the existing customer.

    create:
    Create a new customer instance.

    update:
    Update the gipven customer

    partial_update:
    Update given customer partiallay

    delete:
    Delete the given customer
    """
    queryset = models.Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (IsCompanyMember,IsVerified,IsActive)

    def get_serializer_context(self):
        return {'request': self.request}
    


class OfferViewSet(CompanyAwareView):
    """
    retrieve:
    Return the given offer.

    list:
    Return a list of all the existing offer.

    create:
    Create a new offer instance.

    update:
    Update the given offer

    partial_update:
    Update given offer partiallay

    delete:
    Delete the given offer
    """
    queryset = models.Offer.objects.all()
    serializer_class = OfferSerializer
    permission_classes = (IsCompanyMember,IsVerified,IsOwnerOrReadOnly,IsActive)


class RedeemRequestView(APIView):

    # def get_object(self, pk):
        # try:
            # return models.Customer.objects.get(pk=pk)
        # except models.Customer.DoesNotExist:
            # raise Http404
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=RedeemRequestSerializer ,responses={200: CustomerSerializer})
    def post(self, request, pk, format=None):
        """
          Redeem uisng OTP
        """
        serializer=RedeemRequestSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer = models.Customer.objects.get(
                id=pk,
                company=request.user.company,
                )
            # customer = this.get_object(pk)
            offer = models.Offer.objects.get(
                id=serializer.data['offer_id'],
                company=request.user.company
                )
            if customer.points < offer.points: 
                message = {
                    "message": "Cannot redeem",
                }
                serializer = MessageSerializer(message)
            else:
                customer.otp = randint(000000,999999)
                customer.offer_to_redeem = offer
                customer.save()
                message = "Hi. Thanks for coming at "+ customer.company.name + ". Your otp is " + str(customer.otp) + "."
                sms = send_sms.delay(customer.phone_number,message,request.user.company)
                logger.info(sms)
                serializer = CustomerSerializer(customer)
                return Response(serializer.data)
        else:
            return Response(serializer.errors)


class RedeemView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=RedeemSerializer ,responses={200: MessageSerializer})
    def post(self, request, pk,  format=None):
        """
          Redeem points calculations
        """
      # serializer = RedeemSerializer(request.data)
      # serializer.is_valid()

        customer = models.Customer.objects.get(
            id=pk,
            company=request.user.company
            )
        serializer=RedeemSerializer(data=request.data)
        if serializer.is_valid():
            if customer.otp == serializer.data['otp']:
                customer.points =  customer.points - customer.offer_to_redeem.points
                customer.offer_to_redeem = None
                customer.otp=randint(00000,999999)
                customer.save()
                serializer = CustomerSerializer(customer)
                return Response(serializer.data)
            else:
                message = {
                    "message": "Didnot set otp"
                }
            serializer = MessageSerializer(message)
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class GetPhoneNumber(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=GetPhoneNumberSerializer ,responses={200: CustomerSerializer})
    def post(self,request,format=None):
        serializer=GetPhoneNumberSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer,created = models.Customer.objects.get_or_create(
            phone_number=serializer.data['phone_number'],
            company=request.user.company,
            user=request.user,
            )
            serializer = CustomerSerializer(customer)
            return Response(serializer.data)
        else:
            return Response(serializer.errors)



class SendAllSMSView(APIView):

    """
      Send mass sms  
    """

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=SendAllSmsSerializer(),responses={200: SendAllSmsSerializer()})
    def post(self,request,format=None):
        serializer=SendAllSmsSerializer(data=request.data)
        if serializer.is_valid()==True:
            for customer in request.user.company.customer_set.all():
                send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)        
            return Response({'message':'Success'})
        else:
            return Response(serializer.errors)


class SendSmsToSelected(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsVerified,IsActive)
    @swagger_auto_schema(request_body=SendSmsToSelectedSerializer(),
        responses={200: MessageSerializer()})
    def post(self,request,format=None):
        serializer=SendSmsToSelectedSerializer(data=request.data)
        if serializer.is_valid():
            selected_customer = models.Customer.objects.filter(
                points__gte=serializer.data['min_points'], 
                points__lte=serializer.data['max_points'],
                company=request.user.company,)
            serializer2=SendAllSmsSerializer(data=request.data)
            if serializer2.is_valid()==True:
                sms=serializer2.data['sms']
            else:
                return Response(serializer.errors)

            for customer in selected_customer:
                    send_sms.delay(customer.phone_number,sms,request.user.company)
                    message={'message':'Success'}
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)
        else:
            return Response(serializer.errors)


class CustomerOfferList(GenericAPIView):

    permission_classes = (IsVerified,IsCompanyMember,IsActive)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    @swagger_auto_schema(responses={200: OfferSerializer(many=True)})
    def get(self, request, pk, format=None):
        customer=models.Customer.objects.get(
                id=pk,
                company=request.user.company,
                )
        
        queryset=models.Offer.objects.filter(
            points__lte=customer.points,
            company=request.user.company,
            )
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = OfferSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)



class ItemViewSet(CompanyAwareView):
    """
    retrieve:
    Return the given item.

    list:
    Return a list of all the existing item.

    create:
    Create a new item instance.

    update:
    Update the given item

    partial_update:
    Update given item partiallay

    delete:
    Delete the given item
    """
    queryset = models.Item.objects.all()
    serializer_class = ItemSerializer
    permission_classes = (IsCompanyMember,IsVerified,IsActive)


class MiscellaneousViewSet(CompanyAwareView):
    """
    retrieve:
    Return the given item.

    list:
    Return a list of all the existing item.

    create:
    Create a new item instance.

    update:
    Update the given item

    partial_update:
    Update given item partiallay

    delete:
    Delete the given item
    """
    queryset = models.Miscellaneous.objects.all()
    serializer_class = MiscellaneousSerializer
    permission_classes = (IsCompanyMember,IsVerified,IsActive)
    

class DashboardView(APIView):
    permission_classes = (IsVerified,IsCompanyMember,IsActive)
    @swagger_auto_schema(responses={200: TotalCustomerSerializer})
    def get(self, request, format=None):  
        total_customer = models.Customer.objects.filter(company=request.user.company).count()
        daily_total_customer=models.Customer.objects.filter(Q(company=request.user.company) &
         Q(created__gte=datetime.datetime.now()-datetime.timedelta(days=1)) & 
         Q(created__lte=datetime.datetime.now())).count()

        monthly_total_customer=models.Customer.objects.filter(Q(company=request.user.company) &
         Q(created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) & 
         Q(created__lte=datetime.datetime.now())).count()

        weekly_total_customer = models.Customer.objects.filter(Q(company=request.user.company) &
         Q(created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) & 
         Q(created__lte=datetime.datetime.now())).count()

        buy = models.Cart.objects.filter(Q(company=request.user.company) & 
        Q(created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) & 
        Q(created__lte=datetime.datetime.now())).aggregate(total_sale=Sum('totalprice'))
        
    
        daily_sale=models.Cart.objects.filter(
            company=request.user.company,
            created__date=datetime.date.today(),
        ).aggregate(daily_bill=Sum('totalprice'))

        weekly_total_sale = models.Cart.objects.filter(Q(company=request.user.company) &
         Q(created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) & 
         Q(created__lte=datetime.datetime.now())).aggregate(weekly_bill=Sum('totalprice'))

        total_sale_amount=models.Cart.objects.filter(
            company=request.user.company,
        ).aggregate(total=Sum('totalprice'))

    
        total={
                "total_customer": total_customer ,
                "daily_total_customer":daily_total_customer,
                "monthly_total_customer":monthly_total_customer,
                "weekly_total": weekly_total_customer,
                "daily_total_sale": daily_sale['daily_bill'],
                "weekly_total_sale": round(weekly_total_sale['weekly_bill'],2),
                "monthly_total_sale_amount": round(buy['total_sale'],2),
                "total_sale_amount": round(total_sale_amount['total'],2), 
            }
        serializer=TotalCustomerSerializer(total)
        return Response(serializer.data)


class CampaignPreviewView(APIView):
    permission_classes = (IsCompanyMember,IsVerified,IsActive)
    @swagger_auto_schema(request_body=CampaignPreviewSerializer,responses={200: CampaignOutSerializer})
    def post(self,request,format=None):
        serializer=CampaignPreviewSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer_absent=0
            max_points=serializer.data['max_points']
            min_points=serializer.data['min_points']
            if not (serializer.data['max_points'] and serializer.data['min_points']) is None:
                if serializer.data['customer_absent']== '1_week':
                    customer_absent=models.Customer.objects.filter(
                        Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        ).count()

                if serializer.data['customer_absent']== '2_week':
                    customer_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=2)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        ).count()

                if serializer.data['customer_absent']== '4_week':
                    customer_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        ).count()
            else:

                if serializer.data['customer_absent']== '1_week':
                    customer_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) &
                        ~Q(transaction__created__lte=datetime.datetime.now()))
                        ).count()

                if serializer.data['customer_absent']== '2_week':
                    customer_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=2)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())
                        )).count()

                if serializer.data['customer_absent']== '4_week':
                    customer_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        (~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())
                        )).count()                


            data={
                    'absent_customer': customer_absent,
                }
            serializer=CampaignOutSerializer(data)
            return Response(serializer.data)
        
        else:
             return Response(serializer.errors)
    

class CampaignView(APIView):
    permission_classes = (IsCompanyMember,IsVerified,IsActive)
    @swagger_auto_schema(request_body=CampaignSerializer,responses={200: CampaignOutSerializer})
    def post(self,request,format=None):
        serializer=CampaignSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer_1_week_absent=0
            customer_2_week_absent=0
            customer_4_week_absent=0
            message={'message':'done!'}
            max_points=serializer.data['max_points']
            min_points=serializer.data['min_points']
            
            if not (serializer.data['max_points'] and serializer.data['min_points']) is None:
                if serializer.data['customer_absent']== '1_week':
                    customer_1_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) &
                        ~Q(transaction__created__lte=datetime.datetime.now()) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        )
                    for customer in customer_1_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)


                if serializer.data['customer_absent']== '2_week':
                    customer_2_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=2)) &
                        ~Q(transaction__created__lte=datetime.datetime.now()) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        )
                    for customer in customer_2_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)

                if serializer.data['customer_absent']== '4_week':
                    customer_4_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) &
                        ~Q(transaction__created__lte=datetime.datetime.now()) &
                        Q(points__lte=serializer.data['max_points']) &
                        Q(points__gte=serializer.data['min_points'])
                        )
                    for customer in customer_4_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)
            else:

                if serializer.data['customer_absent']== '1_week':
                    customer_1_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=1)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())
                        )
                    for customer in customer_1_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)

                if serializer.data['customer_absent']== '2_week':
                    print(customer.phone_number)
                    customer_2_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=2)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())
                        )

                    for customer in customer_2_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)

                if serializer.data['customer_absent']== '4_week':
                    customer_4_week_absent=models.Customer.objects.filter(Q(company=request.user.company) &
                        ~Q(transaction__created__gte=datetime.datetime.now()-datetime.timedelta(weeks=4)) &
                        ~Q(transaction__created__lte=datetime.datetime.now())
                        )

                    for customer in customer_4_week_absent:
                        print(customer.phone_number)
                        send_sms.delay(customer.phone_number,serializer.data['sms'],request.user.company)
                        message={'message':'Success'}    
                    serializer=MessageSerializer(message)        
                    return Response(serializer.data)                                    


            data={
                    'message': 'nothing happened'
                }
            serializer=MessageSerializer(data)
            return Response(serializer.data)
        
        else:
             return Response(serializer.errors)


class TransactionView(GenericAPIView):
    permission_classes = (IsCompanyMember,IsVerified,IsActive)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    @swagger_auto_schema(request_body=TransactionInputSerializer ,responses={200: TransactionOutSerializer}) 
    def post(self, request, pk, format=None): #pk=customer id
        serializer=TransactionInputSerializer(data=request.data)
        if serializer.is_valid()==True:
            total=0
            customer = models.Customer.objects.get(
                id=pk,
                company=request.user.company
                )
            transaction=models.Transaction.objects.create(
                customer=customer,
                company=request.user.company)
            transaction.save()
            print("item serializer in before if:"+str(serializer.data['items']))
            if serializer.data['price'] is None:     
                #if price is not given
                if serializer.data['items'] is None: 
                    #if price and item not given
                    msg={'message':'enter a value'} 
                    serializer=MessageSerializer(msg)
                    return Response(serializer.data)
                else:                                  
                    #if item is given
                    print("item serializer in if else:"+str(serializer.data['items']))       
                    for itemObject in serializer.data['items']:
                        #for logging
                        print(str("itemObjectId:")+str(itemObject['item']))
                        get_item=models.Item.objects.get(
                            id=itemObject['item'],
                            company=request.user.company,
                        )
                        if get_item.quantity<itemObject['quantity']:
                            msg={'message':str(get_item.name)+' is not availabe this much'}
                            serializer=MessageSerializer(msg)
                            return Response (serializer.data)
                        else:
                            get_item.quantity=get_item.quantity-itemObject['quantity']
                            get_item.save()
                            cart=models.Cart.objects.create(
                                company= request.user.company,
                                customer= customer,
                                item= get_item,
                                quantity=itemObject['quantity'],
                                transaction = transaction,
                                )       
                            cart.save()
                            total=total+get_item.price
            else:
                #only price is given
                for price in serializer.data['price']:
                    print("price array"+str(price))
                    other_items,_=models.Miscellaneous.objects.get_or_create(
                        price=price,
                        company=request.user.company,
                        )
                    cart=models.Cart.objects.create(
                        customer=customer,
                        miscellaneous_items=other_items,
                        company=request.user.company,
                        transaction=transaction,
                        quantity=1
                        )    
                    cart.save()
                    total=total+other_items.price
                if serializer.data['items'] is None:
                        print("item is not given")
                else:
                    for itemObject in serializer.data['items']:
                        get_item=models.Item.objects.get(
                            id=itemObject['item'],
                            company=request.user.company,
                        )
                        if get_item.quantity<itemObject['quantity']:
                            msg={'message':str(get_item.name)+' is not availabe this much'}
                            serializer=MessageSerializer(msg)
                            return Response (serializer.data)
                        else:
                            get_item.quantity=get_item.quantity-itemObject['quantity']
                            get_item.save()
                            cart=models.Cart.objects.create(
                                company= request.user.company,
                                customer= customer,
                                item= get_item,
                                quantity=itemObject['quantity'],
                                transaction = transaction,
                                )       
                            cart.save()
                            total=total+get_item.price

            cart.totalprice=total
            cart.save()
            total_gst=total*(float(request.user.company.gst_rate)/100)
            total_bill=cart.totalprice+total_gst
            print(total_bill)
            points = total * float(customer.company.conversion_rate)
            customer.points = customer.points + points
            customer.save()
            print(transaction.id)
            print(total)

            items=models.Cart.objects.filter(
                company=request.user.company,
                transaction__id=transaction.id,
                miscellaneous_items__price=None
            ).annotate(                
                name = F('item__name'),
                price = F('item__price'),
                unit = F('item__unit'),
            ).values('name','price','quantity','unit')

            print(items)

            itemSerializer=ItemSerializer(items,many=True)
            other_items=models.Cart.objects.filter(
                company=request.user.company,
                transaction__id=transaction.id,
                item=None
            ).annotate(
                price=F('miscellaneous_items__price')
            ).values('price','quantity')

            print(other_items)
            
            otherItemsSerializer=MiscellaneousSerializer(other_items,many=True)
            

            url=urlShort(requests,'http://'+str(request.get_host())+'/company/'+str(request.user.company.id)+'/transaction/'+str(transaction.id)+'/')
            msg= 'Thanks for shopping with '+str(request.user.company.name)+'. Your Bill is here '+url
            print("reached before send sms")
            try:
                send_sms.delay(customer.phone_number,msg,request.user.company)
            except:
                print("could not send sms")
            final_bill={
                    'transaction_id':transaction.id,
                    'total_gst': round(total_gst,2),
                    'total_price_before_GST':round(cart.totalprice,2),
                    'GST_rate': request.user.company.gst_rate,
                    'total_price_after_GST': round(total_bill,2),
                    'items': itemSerializer.data,
                    'miscellaneous': otherItemsSerializer.data,
                    'company': request.user.company.name,
                    'customer': customer.phone_number,
                    'customer_points': round(customer.points,2),
                    'created': transaction.created,
                    'url':url
                }
            serializer=TransactionOutSerializer(final_bill)
            return Response(serializer.data)
            
        else:
            return Response(serializer.errors)


def getPdfView(request,cpk,pk):
    company=cmodels.Company.objects.get(id=cpk)
    transaction=models.Transaction.objects.get(id=pk)
    cart=models.Cart.objects.filter(
        transaction=transaction,
        company=company
        )
    items=models.Cart.objects.filter(
        company=company,
        transaction__id=transaction.id,
        miscellaneous_items__price=None
        ).annotate(                
            name = F('item__name'),
            price = F('item__price'),
            unit = F('item__unit'),
        ).values('name','price','quantity','unit')
    print(items)
    itemSerializer=ItemSerializer(items,many=True)
    other_items=models.Cart.objects.filter(
        company=company,
        transaction__id=transaction.id,
        item=None
        ).annotate(
            price=F('miscellaneous_items__price')
        ).values('price','quantity')
    print(other_items)
    otherItemsSerializer=MiscellaneousSerializer(other_items,many=True)
    total=0
    for c in cart:
        total=total+c.totalprice
    print(total)
    total_gst=total*(float(company.gst_rate)/100)
    total_bill=total+total_gst

    data={
            'transaction_id':transaction.id,
                'gst_rate':company.gst_rate,
                'before_gst_total': total,
                'total_gst':round(total_gst,2),
                'total_bill':round(total_bill,2),
                    'items': itemSerializer.data,
                    'miscellaneous': otherItemsSerializer.data,
                    'company': company.name,
                'customer': transaction.customer.phone_number,
            'customer_points': transaction.customer.points,
        'created': transaction.created,
        }
    pdf = render_to_pdf('pdf/invoice.html', data)
    return HttpResponse(pdf, content_type='application/pdf')


class BillManagement(GenericAPIView):
    permission_classes = (IsVerified,IsCompanyMember,IsActive)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    @swagger_auto_schema(request_body=TransactionIdSerializer,responses={200: MessageSerializer})
    def post(self, request, format=None):
        serializer=TransactionIdSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer=models.Customer.objects.get(
                transaction__id=serializer.data['transaction_id'],
                company=request.user.company
                )
            serializer=MessageSerializer(
                {'message':'http://'+str(request.get_host())+'/company/'+str(request.user.company.id)+'/transaction/'+str(serializer.data['transaction_id'])+'/'})
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
    
    @swagger_auto_schema(request_body=TransactionIdSerializer,responses={200: MessageSerializer})
    def delete(self, request, format=None):
        serializer=TransactionIdSerializer(data=request.data)
        if serializer.is_valid()==True:
            transaction=models.Transaction.objects.get(
                id=serializer.data['transaction_id'],
                company=request.user.company)
            transaction.is_deleted=True
            transaction.save()
            serializer=MessageSerializer({"message":"Deleted!!"})
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
    
    @swagger_auto_schema(responses={200: TransactionSerializer})
    def get(self, request, format=None):
        transaction=models.Transaction.objects.filter(
            is_deleted=False,
            company=request.user.company)
        serializer=TransactionSerializer(transaction, many=True)
        return Response(serializer.data)


class SendSmstoPerticularCustomerView(APIView):
    permission_classes=(IsVerified,IsCompanyMember,IsActive)
    @swagger_auto_schema(request_body=SendAllSmsSerializer, responses={200: MessageSerializer})
    def post(self,request,pk,format=None):
        serializer=SendAllSmsSerializer(data=request.data)
        if serializer.is_valid()==True:
            customer=models.Customer.objects.get(id=pk)
            send_sms.delay(
                customer.phone_number,
                serializer.data['sms'],
                request.user.company
                )
            msg=serializer.data['sms']+' sent to '+str(customer.phone_number)
            serializer=MessageSerializer({"message":msg})
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class TopOffersSendtoACustomer(APIView):
    permission_classes=(IsVerified,IsCompanyMember,IsActive)
    @swagger_auto_schema(responses={200: SendSmstoPerticularCustomerSerializer})
    def post(self,request,pk,format=None):
        new_msg='Dear customer, '
        msg=''
        customer=models.Customer.objects.get(id=pk)
        offers=models.Offer.objects.filter(
            company=request.user.company,
            points__lte=customer.points).order_by('-points')
        print(offers)
        for offer in offers:
            msg=new_msg
            print(msg)
            print(offer.about)
            new_msg=new_msg+str(offer.about)+'. '
            if len(msg)>=140:
                break
        print(msg)
        print(len(msg))
        send_sms.delay(customer.phone_number, msg ,request.user.company)
        data={
            'sms':msg,
            'phone_number':customer.phone_number,
        }
        serializer=SendSmstoPerticularCustomerSerializer(data)
        return Response(serializer.data)

# ===============================v1.0.0======================================


class QuoteView(APIView):
    '''
    Ouote is a button in customer view board:
    - On pressing a popup comes up
    - You can select the item whose price you want to send. 
    - When clicked ok then sends a SMS which contains the
     product details and price. 
     ( eg. Samsung Original EHS64 EHS64AVFWECINU Wired Stereo Headset
      (White) for just Rs. 300 )

    '''
    permission_classes=(IsVerified,IsCompanyMember,IsActive)
    @swagger_auto_schema(request_body=ItemIdSerializer ,responses={200: SendSmstoPerticularCustomerSerializer})
    def post(self,request,pk,format=None):
        customer=models.Customer.objects.get(id=pk)
        serializer=ItemIdSerializer(data=request.data)
        if serializer.is_valid()==True:
            item=models.Item.objects.get(id=serializer.data['item_id'])
            msg='Dear customer, '+item.name+' is for just Rs.'+str(item.price)+'. '
            print(msg)
            print(len(msg))
            if len(msg)<=140:
                send_sms.delay(customer.phone_number,msg,request.user.company)
                data={
                'sms':msg,
                'phone_number':customer.phone_number,
                }
                serializer=SendSmstoPerticularCustomerSerializer(data)
                return Response(serializer.data)
            else:
                serializer=MessageSerializer({"message":"Reduce the message length"})
                return Response(serializer.data)
        else:
            return Response(serializer.errors)


class CategoryViewSet(CompanyAwareView):
    queryset=models.Category.objects.all()
    serializer_class= CategorySerializer
    permission_classes=(IsVerified,IsCompanyMember,IsActive)

class CategoryManagement(GenericAPIView):
    permission_classes=(IsVerified,IsCompanyMember,IsActive)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    @swagger_auto_schema(responses={200: ItemSerializer})
    def get(self,request,pk,format=None):
        queryset=models.Item.objects.filter(
            category__id=pk,
            company=request.user.company
            )
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer=ItemSerializer(queryset,many=True)
            return self.get_paginated_response(serializer.data)


class NoteView(GenericAPIView):
    permission_classes=(IsVerified,IsCompanyMember,IsActive)
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    @swagger_auto_schema(request_body=NoteSerializer, responses={200: NoteSerializer})
    def post(self,request,pk,format=None):
        customer=models.Customer.objects.get(id=pk)
        serializer=NoteSerializer(data=request.data)
        if serializer.is_valid()==True:
            note=models.Note.objects.create(
                company=request.user.company,
                note=serializer.data['note'],
                customer=customer,
            )
            note.save()
            serializer=NoteSerializer(note)
            return Response(serializer.data)
        else:
            return Response(serializer.errors)

    @swagger_auto_schema(responses={200: NoteSerializer})
    def get(self,request,pk,format=None):
        customer=models.Customer.objects.get(id=pk)
        note=models.Note.objects.filter(
                company=request.user.company,
                customer=customer,
            ).order_by('-created')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer=NoteSerializer(note,many=True)
            return self.get_paginated_response(serializer.data)
        return Response(serializer.data)



class ProductSuggest(GenericAPIView):
    def post(self,request,pk,format=None):
        customer=models.Customer.objects.get(id=pk)
        category=models.Cart.objects.filter(
            company=request.user.company,
            customer=customer,
        ).values('item__category__id')
        print(category)
        selected_category=mostFrequent(category)
        items=models.Item.objects.filter(
            company=request.user.company,
            category__id=selected_category
            )
        random_item_id=random.choice(items)
        msg=str(item.name)+' at just Rs.'+str(item.price)+' .Grab now from '+str(request.user.company.name)
        send_sms.delay(customer.phone_number,msg,request.user.company)
        serializer=MessageSerializer({"message":msg})
        return Response(serializer.data)


        
