from rest_framework.test import APITestCase
from ..models import Customer,Offer,CompanyAwareModel,Message
from ...company.models import Company,Package
import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..serializers import CustomerSerializer,OfferSerializer,RedeemRequestSerializer,SendAllSmsSerializer,SendSmsToSelectedSerializer
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from django.conf import settings
from rest_framework.test import APIClient
from ..views import CustomerViewSet,OfferViewSet
from rest_framework.authtoken.models import Token
from ...users.models import User
from django.test import TestCase
from mixer.backend.django import mixer
from . import data
import datetime

class Company1(APITestCase):
    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages=10,
            price='0',
            total_days='14',
            number_of_pos_operators=1,)
        self.package.save()
        self.company=Company.objects.create(
            name ='Another World',
            phone_number = '7278684318',
            package = self.package,
            messages_left = self.package.total_messages,
            is_veryfied=True,
            valid_upto=datetime.date.today())
        self.company.save()
        self.user=User.objects.create_user('pabi',password='lala',
            company=self.company,
            is_owner=True,)
        self.user.save()
        self.client = APIClient()
        token = Token.objects.get(user=self.user) 
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)


class Company2(APITestCase):
    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages=10,
            price='0',
            total_days='14',
            number_of_pos_operators=1)
        self.package.save()
        self.company=Company.objects.create(
            name ='Company2',
            phone_number = '6290990312',
            package = self.package,
            messages_left = self.package.total_messages,
            is_veryfied=True,
            valid_upto=datetime.date.today())
        self.company.save()
        self.user=User.objects.create_user('pabi',password='lala',
            company=self.company,
            is_owner=True,)
        self.user.save()
        self.client = APIClient()
        token = Token.objects.get(user=self.user) 
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        
class CustomerTestCaseCompany1(Company1):
        
    def test_customer_create(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['valid'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_case_customer_create_null_value(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['null'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_customer_create_with_character_phone_number(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['chracter_phone_number'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_customer_create_with_invalid_email(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['invalid_email'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_customer_delete(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['valid'], 
            format='json')

        customer=Customer.objects.get(username='pabi')
        url='http://127.0.0.1:8000/api/v1/customer/'+str(customer.id)+'/'
        response = self.client.delete(
            url,format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_case_customer_patch(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.post(
            url,
            data.customer['valid'], 
            format='json')

        customer=Customer.objects.get(phone_number='7278684318')
        url = 'http://127.0.0.1:8000/api/v1/customer/'+str(customer.id)+'/'
        data1 = {
        'phone_number':'7278684317', 
        'username':'pabiyy',
        'email':'pabiyy@gmail.com',
        }
        response = self.client.patch(
            url,
            data1, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_customer_put(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid'], 
            format='json')
        customer=Customer.objects.get(phone_number='7278684318')
        url = 'http://127.0.0.1:8000/api/v1/customer/'+str(customer.id)+'/'
        response=self.client.put(
            url,
            data.customer['less_number_phone'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    '''def total_customer_count_test_case(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid'], 
            format='json')

        self.client.post(
            url,
            data.customer['valid2'], 
            format='json')
        url='http://127.0.0.1:8000/api/v1/stat/'
        response=self.client.get(
            url,
            format='json')
        self.assertEqual(2 , int(response.data['total_customer']))'''
        


class CustomerTestCaseCompany2(Company2):
        
    def test_customer_get(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.get(
            url, 
            format='json')
        self.assertEqual(response.data['count'], 0)
    
    '''def total_customer_count_test_case(self):
        url='http://127.0.0.1:8000/api/v1/stat/'
        response=self.client.get(
            url,
            format='json')
        self.assertEqual(0 , int(response.data['total_customer']))'''


class OfferTestCaseCompany1(Company1):
    """
    1. create all the database necessary 
    2. then call the api
    """
    def test_create_offer(self):
        """
        Ensure we can create a new account object.
        """
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        response = self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_offer_with_negetive_points(self):
        """
        Ensure we can create a new account object.
        """
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        response = self.client.post(
            url,
            data.offer['negetive_points'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_offer_with_character_points(self):
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        response = self.client.post(
            url,
            data.offer['char_points'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_offer(self):

        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        offer=Offer.objects.get(points='10')
        url = 'http://127.0.0.1:8000/api/v1/offer/'+str(offer.id)+'/'
        response = self.client.get(
            url,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_delete_offer(self):

        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        offer=Offer.objects.get(points='10')
        url = 'http://127.0.0.1:8000/api/v1/offer/'+str(offer.id)+'/'
        response = self.client.delete(
            url,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


    def test_put_offer(self):
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        offer=Offer.objects.get(points='10')
        url = 'http://127.0.0.1:8000/api/v1/offer/'+str(offer.id)+'/'
        response=self.client.put(
            url,
            data.offer['valid2'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_patch_offer(self):
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        offer=Offer.objects.get(points='10')
        url = 'http://127.0.0.1:8000/api/v1/offer/'+str(offer.id)+'/'
        data2 = {'about':'50 pts offer!! yooo',}
        response=self.client.patch(
            url,
            data2, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class OfferTestCaseCompany2(Company2):
    def test_get_offer(self):
        offer=Offer.objects.all().order_by('-created_at')
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        response = self.client.get(
            url,
            format='json')
        self.assertEqual(response.data['count'], 0)




class RedeemRequestViewTestCases(Company1):

    def sucessfull_redeem_test_case(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid'], 
            format='json')
        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        customer=Customer.objects.get(username='pabi')
        offer=Offer.objects.get(points='10')
        url='http://127.0.0.1:8000/api/v1/customer/'+str(customer.id)+'/redeem/'
        data1={'offer_id':offer.id ,}
        response = self.client.post(
            url,
            data1, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class RedeemViewTestCase(Company1):

    def redeeming_with_customer_points_is_morethan_offer_points(self): 
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid2'], 
            format='json')

        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid'], 
            format='json')
        self.customer=Customer.objects.get(username='pabi')
        self.offer=Offer.objects.get(points='10')

        url='http://127.0.0.1:8000/api/v1/customer/'+str(self.customer.id)+'/redeem/otp/'
        data1={'otp':'136480',}
        response = self.client.post(
            url,
            data1, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetPhoneNumberTestCase(Company1):

    def get_phone_number_test_case(self):
        url='http://127.0.0.1:8000/api/v1/customer/phone'
        data={'phone_number':'7980573770'}
        response = self.client.post(
         url,
         data, 
         format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetPhoneNumberTestCompany1(Company2):

    def get_phone_number_test_case(self):
        url='http://127.0.0.1:8000/api/v1/customer/'
        response = self.client.get(
         url,
         format='json')
        self.assertEqual(response.data['count'], 0)

class SendAllSMSViewtestcase(Company1):
        
    def send_all_sms(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid'], 
            format='json')

        self.client.post(
            url,
            data.customer['valid2'], 
            format='json')

        url='http://127.0.0.1:8000/api/v1/customer/phone'
        data1={'sms':'hello'}
        response = self.client.post(
         url,
         data1, 
         format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class SmsToSelectedTestCase(Company1):

    def send_sms_to_selected_test_cases(self):
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        data = {
        'phone_number':'9614947956', 
        'points':'20',}
        self.client.post(
            url,
            data, 
            format='json')
        data = {
        'phone_number':'7980573770', 
        'points':'15',}
        self.client.post(
            url,
            data, 
            format='json')
        data = {
        'phone_number':'7278684318', 
        'points':'10',}
        self.client.post(
            url,
            data, 
            format='json')
        data = {
        'phone_number':'9903796626', 
        'points':'5',}
        self.client.post(
            url,
            data, 
            format='json')
        url='http://127.0.0.1:8000/api/v1/sms/'
        data={'max_points':'15','min_points':'10','sms':'hey' }
        response=self.client.post(
            url,
            data, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CustomerOfferListTestCases(Company1):
    
    def customer_offer_list_test_case(self):    
        url = 'http://127.0.0.1:8000/api/v1/customer/'
        self.client.post(
            url,
            data.customer['valid'], 
            format='json')

        url = 'http://127.0.0.1:8000/api/v1/offer/'
        self.client.post(
            url,
            data.offer['valid2'], 
            format='json')
        data1 = {'points': '5','about':'5 pts offer',}
        self.client.post(
            url,
            data1, 
            format='json')
        data2 = {'points': '25','about':'25 pts offer',}
        self.client.post(
            url,
            data2, 
            format='json')
        customer=Customer.objects.get(username='pabi')
        offer=Offer.objects.all()
        url='http://127.0.0.1:8000/api/v1/customer/'+str(customer.id)+'/offer/'
        response=self.client.get(
            url, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)



class ItemTestCase(Company1):

    def item_post_test_case(self):
        url='http://127.0.0.1:8000/api/v1/items/'
        data={'name':'Car','price':'20000'}
        response=self.client.post(
            url,
            data, 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class CompanyUserTestCase(APITestCase):
    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages='0',
            price='0',
            total_days='14',
            number_of_pos_operators=5)
        self.company=Company.objects.create(name='pabi',phone_number='7278684318',package=self.package)
        self.company.save()
        self.user=User.objects.create_user('pabi',password='lala',company=self.company)
        self.user.save()
        self.client = APIClient()

    def TestCasecreateCompanyandUserFullData(self):
        url='http://127.0.0.1:8000/api/v1/register/'
        response=self.client.post(
            url,
            data.reg['full'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def TestCasecreateCompanyandUserMinimalData(self):
        url='http://127.0.0.1:8000/api/v1/register/'
        response=self.client.post(
            url,
            data.reg['half'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AddPosOperatorTestCase(Company1):

    def test_case_add_pos_operator(self):
        url='http://127.0.0.1:8000/api/v1/posoperator/'
        response=self.client.post(
            url,
            data.user['valid'], 
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

#unit test case

class CustomerUnitTestCase(TestCase):

    def setUp(self):
        self.package=Package.objects.create(
            name='Trial pack',
            total_messages='1',
            price='0',
            total_days='14',
            number_of_pos_operators=1,)
        self.package.save()
        self.company=Company.objects.create(
            name='pabi',
            phone_number='7278684318',
            package=self.package)
        self.company.save()
        self.user=User.objects.create_user('pabi',password='lala',company=self.company)
        self.user.save()
        self.customer=Customer.objects.create(
            company=self.user.company,
            phone_number='7278684318',
            username='pabiy',
            email='popo@popo.po',
            points='18',
            )
        self.customer.save()

    def test_customer_phone_get(self):
        get_customer=Customer.objects.get(username='pabiy')
        self.assertEqual(get_customer.phone_number, '7278684318')

    def test_customer_username_get(self):
        get_customer=Customer.objects.get(phone_number='7278684318')
        self.assertEqual(get_customer.username, 'pabiy')

    def test_customer_company_get(self):
        get_customer = Customer.objects.get(username='pabiy')
        self.assertEqual(get_customer.company, self.user.company)

    def test_customer_email_get(self):
        get_customer=Customer.objects.get(username='pabiy')
        self.assertEqual(get_customer.email, 'popo@popo.po')

    def test_customer_email_get(self):
        get_customer=Customer.objects.get(username='pabiy')
        self.assertEqual(get_customer.points, 18)