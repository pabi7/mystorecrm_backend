from rest_framework import permissions
import datetime

class IsCompanyMember(permissions.BasePermission):
    """
        Object-level permission to only allow owners of an object to edit it.
    """
    def has_object_permission(self, request, view, obj):
        return obj.company == request.user.company


class IsSameCompanyOwner(permissions.BasePermission):
    """
        Object-level permission to only allow owners of an object to edit it.
    """
    def has_object_permission(self, request, view, obj):
        return (obj.company and request.user.company and request.user.company.is_owner)


class IsVerified(permissions.BasePermission):
	"""
		Object-level permission, to allow access to all apis, except registration;
		only to verified company members.
	"""
	message = 'This company is not verified!'
	def has_permission(self, request, view):
		return bool(request.user.company and request.user.company.is_veryfied )


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in ('GET','HEAD','OPTIONS'):
            return True

        # permitting if below returns true.
        return bool(request.user and request.user.is_owner)


class IsOwnerOrNothing(permissions.BasePermission):

    def has_permission(self, request, view):
        #permitting everything if instance is owner
        return bool(request.user and request.user.is_owner)


class IsActive(permissions.BasePermission):

    message='Package validity expired! Recharge now!!'
    def has_permission(self, request, view):
        return bool(request.user.company.valid_upto >= datetime.date.today())


class PosOperatorLimit(permissions.BasePermission):
    
    message='You cannot add more operators! Recharge higher package!!'
    def has_permission(self, request, view):
        return bool(request.user.company.number_of_pos_operators > 0)
