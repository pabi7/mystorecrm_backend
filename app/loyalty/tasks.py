from celery import shared_task
from random import randint
import http 
from ..company.models import Company
import razorpay
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.conf import settings
import random, string
import requests



def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)


def randomword(length):
   letters = string.ascii_uppercase
   return ''.join(random.choice(letters) for i in range(length))
   

def customized_sender_name(comp):
    company=str(comp)
    if len(company)> 6:
        name=company[:6]
        return name.upper()
    else:
         return (str(company)+str(randomword((6-len(company))))).upper()

@shared_task(serializer='pickle')
def send_sms(phone_number,message,company):
    company_messages = company.messages_left
    if (company_messages>0):
        conn = http.client.HTTPConnection("api.msg91.com")
        msg=str(message)
        conn.request("GET","/api/sendhttp.php?sender="+str(customized_sender_name(company.name))+"&route=4&mobiles="+str(phone_number)+"&authkey="+str(settings.MSG91_KEY)+"&country=91&message="+str(msg))
        res = conn.getresponse()
        data = res.read()
        company.messages_left=int(company_messages)-1
        company.save()
        return data.decode("utf-8")
    else:
        raise Exception ("No messages left, Please recharge a SMS pack!")

@shared_task        
def admin_send_sms(phone_number,message,company):
    conn = http.client.HTTPConnection("api.msg91.com")
    msg=str(message)
    conn.request("GET","/api/sendhttp.php?sender=MYSTOR&route=4&mobiles="+str(phone_number)+"&authkey="+str(settings.MSG91_KEY)+"&country=91&message="+str(msg))
    res = conn.getresponse()
    data = res.read()
    return data.decode("utf-8")


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None
    
@shared_task 
def urlShort(requests,params):
    url='http://thelink.la/api-shorten.php?url='+str(params)
    res=requests.get(url = url,)
    print(res.text)
    return str(res.text)
    

    