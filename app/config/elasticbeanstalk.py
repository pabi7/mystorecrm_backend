import os
from distutils.util import strtobool
from .common import Common
from .common import BASE_DIR

class Elasticbeanstalk(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    INSTALLED_APPS += ("gunicorn", )
    DEBUG = strtobool(os.getenv('DJANGO_DEBUG', 'no'))
    MSG91_KEY=os.getenv('MSG91_KEY','245552AnZz1wkBa21N5bd98df2')
