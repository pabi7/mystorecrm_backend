from .local import Local  # noqa
from .production import Production  # noqa
from .heroku import Heroku
from .elasticbeanstalk import Elasticbeanstalk
