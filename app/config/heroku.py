import os
from distutils.util import strtobool
from .common import Common


class Heroku(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    INSTALLED_APPS += ("gunicorn", )
    DEBUG = strtobool(os.getenv('DJANGO_DEBUG', 'yes'))
