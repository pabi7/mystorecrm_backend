#!/bin/bash

DIR=~/niharon_backend/
LOGDIR=~/log/

LOGFILE=$LOGDIR'gunicorn.log'
ERRORFILE=$LOGDIR'gunicorn_error.log'

NUM_WORKERS=3

ADDRESS=127.0.0.1:8000

source ~/command/env.sh
source $DIR'active.sh'

exec /home/ubuntu/.virtualenvs/env/bin/gunicorn config.wsgi:application \
--env DJANGO_SETTINGS_MODULE=config.settings.prod \
-w $NUM_WORKERS --bind=$ADDRESS \
--log-level=debug \
--access-logfile $LOGFILE \
--error-logfile $ERRORFILE
