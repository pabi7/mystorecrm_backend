#!/bin/sh
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib nginx python3 python3-dev 
sudo apt-get install supervisor postgis python-certbot-nginx
sudo apt install libcurl4-openssl-dev libssl-dev # curl 
pip install supervisor virtualenv virtualenvwrapper

