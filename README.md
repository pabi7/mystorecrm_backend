MystoreCRM (v1.0.1)
============

Installation 
------------
* Local 
* Heroku 
* Banstalk ( AWS )

Deployment Enviroments
----------------------
* heroku
* aws
* Beanstalk

Celery Worker
------------
* celery -A app worker -l info

Beanstalk
---------
* sudo /opt/elasticbeanstalk/bin/get-config environment --output YAML
* source /opt/python/run/venv/bin/activate
* cd /opt/python/current/app
* python manage.py createsuperuser

API Docs ( /api/v1/ )
---------------------
1. /api-token-auth – User login (open)
	* req
		* username
		* passowrd
	* res
		* token

2. /api/v1/register/ (auth not requried)
	* req
		* name
		* phone
		* email
		* website
		* gst – not mandatary
		* points_value → 1 point = how many rs
		* user_name
		* user_passowrd
	* res
		* company

4. /api/v1/password/forgot/
	* req
		* phone_number
	*res
		* Sms send successfully

3. /api/v1/posoperator/
	* req
		* username
        * phonne number
		* password	
	* res
		* user

4. /user/ ( post )  (to be deleted)

5. /user/{id}/ ( put / patch ) (to be deleted)

6. /customer/ - CRUD
	* id
    * company
    * phone_number
    * username
    * email
    * points 

7. /customer/phone/ - get or create customer
	* req
		* phone_number
	* res
		* cutomer body  

8. /customer/{id}/offer/ - get
	*req
		(none)
	* res
		* offer body

9. /offer - <CRUD>
	* points
	* about

10. /package - <RL>
	* name
	* price
	* total_days
	* total_messages

11. /customer/{id}/buy/
	* req
		* price
	* res
		* cusomer body

12. /customer/{id}/redeem/
	* req
		* offer_id
	* res
		* customer body

12. /customer/{id}/redeem/otp/
	* req
		* otp
	* res
		* customer body

13. /sms/all/
	* req
		* sms
	* res
		* { message: success }

14. /sms/
	* req
		* min
		* max
	* res
		* { message: success }
[  New  ]

15. /customer/{id}/offer/ (get)
	* res: only the offers that are relevant to the customer

16. /customer/{id}/status/(get) (**not implimented)
	* res:
 		* last_buy
		* frequeny
		* total_buy

17. /api/v1/stat/ (get)
	* res:
		* total number of customer
		* weekly total customer
		* monthly total customer
		* total revenue in a month

19. /api/v1/posoperator/
	* req
		* username
		* password
		* email
	*res
		* userdata

20. /api/v1/otpcheck/
	* req
		* otp
	* res
		* token

21. api/v1/recharge/{str:payment_id}/ 
	* req
		* package_id
	* res
		* company

22.  /api/v1/sms/all/
	* req
		* sms (text to send)
	* res
		* {messsage: success}

23. /api/v1/customer/{customer_id}/offer/
	* req
		* (none)
	* res
		* relavant offer to the customer

24. /api/graph/
	*res
		*Swagger UI

25. /company - <CRUD>
	* phone_number
	* name
	* address
	* website
	* email
	* phone_number
	* gstin
	* message_left
	* is_active
	* conversion_rate
	* valid_upto
	* otp
	* is_verified

26. api/v1/items/ - <CRUD>
	* id
	* name
	* price
	* quantity

27. api/v1/customer/{customer_id}/transaction/
	* atleast one field is mandatory in request
	* req
		* item array
		* price array
	* res
		* company
		* customer_phone_number
		* Billing Date
		* item name
		* item price
		* total price
		* gst rate
		* after gst total price

28. /company/{company_id}/customer/{customer_id}/
		*res
			* pdf bill

29. api/v1/customsms/
	* req
		* max_points
		* min_points
		* customer_absent
	* res
		* success

30. api/v1/posregister/
	* req
		* phone
		* username
	* res
		* sms
		* phone number
		* username

31. api/v1/posoperator/
	* req
		* username
		* password
		* email
	* res
		* user_body

32. api/v1/pos/ (RULD)


33. api/v1/company/
	* res
		* company_details 

34. api/v1/company/edit/
	* req
        * name
        * address
        * website
        * email
        * gstin
        * conversion_rate
	* res
		* company_body

35. api/v1/password/forgot/
	* req
		* phone_number
	* res
		* sms

36. api/v1/password/reset/
	* req
		* one time password
	* res
		* User_body

37. api/v1/company/
	* res
		* company's own body


38. api/v1/company/edit/
	* req
		* name
        * address
        * website
        * email
        * gstin
        * conversion_rate
	* res
		* company_body

39. api/v1/category/ (CRUD)

	** items adding **
40. api/v1/additems/category/
	* req
		* item[]
		* category[]
	* res
		* catagory
		* inserted items

	** items moving **
41.	api/v1/moveitems/category/
	* req
		* item[]
		* categoty[]
	* res
		* items
		* new categories

	** items copy **
42. api/v1/copyitems/category/
	* req
		* item[]
		* category[]
	* res
		* items
		* catagories
  
 Models
 -------

 1. Company
 	* package
 	* phone_number
	* name
	* address
	* website
	* email
	* phone_number
	* gstin
	* message_left
	* is_active
	* conversion_rate
	* valid_upto
	* otp
	* is_varified
	* offer_to_redeem
	* number_of_pos_operators

2. User
	* id
	* company
	* website
	* email
	* is_owner
	* otp
	* username
	* passowrd
	* last_login
	* is_superuser
	* first_name
	* last_name
	* is_staff
	* is_active
	* date_joined

3. Customer
	* id
    * company
    * phone_number
    * username
    * email
    * points
    * otp
    * offer_to_redeem

4. Offer
	* company
	* about
	* points 

5. Message
	* company
	* text


7. Package
	* total_messages
    * price
    * total_days
    * name
	* number_of_pos_operators

8. Category
	* name

9. Item
	* name
	* price
	* quantity
	* unit

10. Mislleneous item
	* price

11. Note
	* note


v0.1.0 
------
Was for Demo.
* Without Multencency


v0.2.0
------
*  Database Breaks
*  Multiteneny 

v0.2.3
------
* Admin dashboard
* AI restriction upon company is verified or not
* Several Bug Fixes

v0.3
----
* Inventory 
* Password Recovery OTP

v0.4
----
* GST Billing


v1.0.1
------
* pos operator workflow
* send sms to a perticular customer
* send top offers to a customer

v1.0.2
-------
* migrations are deleted, database reset
* read me added
* quote added

v1.0.3
-------
* server fix

v1.0.4
-------
* dump data of package added
* category
* url short
* demand
* billing url issue solved

v1.0.5
------
* database change 

v1.0.6
------
* createmaster change

v1.0.7
-------
* server fix

v1.0.8
-------
* server fix

v1.0.9
-------
* server fix

v1.0.10
---------
* otp is changing after work done


v1.0.11
--------
* if company's any field got duplicate value then it shows error json,
	and nothing happen in database

v1.1.0
--------
* Note feature added
* item models's stock field changed into quantity
* inventory management system added
* url shortner added
* transaction api wants item id and quantity
* buy api,model,serializer removed 
